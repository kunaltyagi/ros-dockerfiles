#! /usr/bin/env sh

build_images() {
    prefix=${1:-kunaltyagi}
    name=${2:-melodic}
    for file in *-${name}-*.dockerfile; do
        name=`echo $file | cut -d '.' -f1`
        docker build --pull -t ${prefix}/ros-dev$name - < $file
    done
}
build_images $1 $2
