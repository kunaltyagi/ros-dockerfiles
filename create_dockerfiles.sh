#! /usr/bin/env sh

parse_yaml() {
    local yaml_file=$1
    local prefix=$2
    local s='[[:space:]]*'
    local w='[a-zA-Z0-9_.-]*'
    local fs="$(echo @|tr @ '\034')"

    (
        sed -e '/- [^\“]'"[^\']"'.*: /s|\([ ]*\)- \([[:space:]]*\)|\1-\'$'\n''  \1\2|g' |

        sed -ne '/^--/s|--||g; s|\"|\\\"|g; s/[[:space:]]*$//g;' \
            -e "/#.*[\"\']/!s| #.*||g; /^#/s|#.*||g;" \
            -e "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
            -e "s|^\($s\)\($w\)${s}[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" |

        awk -F"$fs" '{
            indent = length($1)/2;
            if (length($2) == 0) { conj[indent]="+";} else {conj[indent]="";}
            vname[indent] = $2;
            for (i in vname) {if (i > indent) {delete vname[i]}}
                if (length($3) > 0) {
                    vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
                    printf("%s%s%s%s=\"%s\"\n", "'"$prefix"'",vn, $2, conj[indent-1],$3);
                }
            }' |

        sed -e 's/_=/=/g' |

        awk 'BEGIN {
                FS="=";
                OFS="="
            }
            /(-|\.).*=/ {
                gsub("-|\\.", "_", $1)
            }
            { print }'
    ) < "$yaml_file"
}

key_value_pair() {
    echo $1 | awk -F'[="]' '{print $1 " " $3}'
}

contains() {
    echo $1 | egrep -qi "(^|[[:space:]])$2($|[[:space:]])"
    return $?
}

os_type() {
    local codename=$1
    local debian="bullseye buster stretch jessie wheezy"
    local ubuntu_lts="bionic xenial trusty precise"

    local name="unknown"
    if contains "$debian" $codename; then
        name="debian"
    elif contains "$ubuntu_lts" $codename; then
        name="ubuntu"
    fi
    echo -n $name
}

prereq() {
    local os=$1
    if [ "$os" = "debian" ]; then
        echo -n "\
echo -n 'deb http://cdn-fastly.deb.debian.org/debain testing main contrib non-free' > /etc/apt/sources.list.d/testing.list\
"
    elif [ "$os" = "ubuntu" ]; then
        echo -n "\
apt update && apt install -y software-properties-common > /tmp/log && \
add-apt-repository -y ppa:ubuntu-toolchain-r/test > /tmp/log && \
apt update > /tmp/log && apt upgrade -y > logs || echo /tmp/log
"
    fi
}

apt_dependencies() {
    echo -n "build-essential g++-8 gcc-8 git"
}

apt_prefix() {
    local os=$1
    if [ "$os" = "debian" ]; then
        echo -n "apt install -y -t testing"
    elif [ "$os" = "ubuntu" ]; then
        echo -n "apt install -y"
    fi
}

apt_cleanup() {
    echo -n 'update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 60 --slave /usr/bin/g++ g++ /usr/bin/g++-8 && \
rm -fr /var/lib/apt/lists/'
}

source_dependencies() {
    # cmake
    echo -n "\
cd /tmp && git clone --depth=1 https://github.com/Kitware/CMake.git && \
cd CMake && ./bootstrap > /tmp/log && make -j4 > /tmp/log && \
make install -j4 > /tmp/log && cd /tmp && rm -fr CMake || echo /tmp/log
"
}

indent() {
    local text="$1"
    local width=$2
    local char=${3:-" "}

    local replacement=`seq -s= $width | tr -d '[:digit:]' | tr '=' "$char"`
    echo -n $text | sed "2,$ s/^/$replacement/"
}

get_image_name() {
    local release=$1
    local flavor=$2
    local os=$3

    local image_name="ros:${release}-${flavor}-${os}"
    echo -n $image_name
}

generate_content() {
    local release=$1
    local flavor=$2
    local codename=$3

    local os=`os_type $codename`
    local image_name=`get_image_name $release $flavor $codename`

    local _prereq="`prereq $os`"
    local _apt_prefix="`apt_prefix $os`"
    local _apt_pkg="`apt_dependencies $os`"
    local _apt_clean="`apt_cleanup`"
    local _src="`source_dependencies`"
    echo -n "\
FROM ${image_name}

LABEL maintainer = 'Kunal Tyagi <tyagi.kunal@live.com>' \\
      description = 'development docker for ROS image {name}'

RUN `indent \"$_prereq\" 4`
RUN $_apt_prefix `indent \"$_apt_pkg\" 4` && \\
    `indent \"$_apt_clean\" 4`
RUN `indent \"$_src\" 4`
"
}

get_list() {
    local yaml="$1"
    local name=$2
    local items=""
    for i in $yaml; do
        local kv_pair="`key_value_pair $i`"
        local key=`echo $kv_pair | awk '{print $1}'`
        local value=`echo $kv_pair | awk '{print $2}'`

        if contains "$key" $name; then
            items="`echo $items $value`"
        fi
    done
    echo -n $items
}
get_map() {
    local yaml="$1"
    local name=$2
    local items=""
    for i in $yaml; do
        local kv_pair="`key_value_pair $i`"
        local key_map_pair="`echo $kv_pair | tr '_' ' '`"
        # local key=`echo $key_map_pair | awk '{print $1}'`
        local key="`echo $i | cut -f1 -d '_'`"
        if [ "$name" != "$key" ]; then
            continue
        fi
        local map_value="`echo $i | cut -f2- -d '_'`"
        items="`echo $items $map_value`"
    done
    echo -n $items
}

generate_dockerfiles() {
    local config=$1
    local yaml="`parse_yaml $config`"

    local flavors="`get_list \"$yaml\" flavors`"

    for flavor in $flavors; do
        local releases="`get_map \"$yaml\" releases`"

        for release in $releases; do
            local kv_pair="`key_value_pair $release`"
            local release=`echo $kv_pair | awk '{print $1}'`
            local os=`echo $kv_pair | awk '{print $2}'`
            generate_content $release $flavor $os > "ros-${release}-${flavor}-${os}-dev.dockerfile"
        done
    done
}

generate_dockerfiles $1
